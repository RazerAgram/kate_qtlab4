#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QFileDialog>
#include <QTextStream>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_lineEdit_textChanged(const QString &arg1)
{
    ui->lineEdit_2->setText(arg1);
}

void MainWindow::on_lineEdit_2_textChanged(const QString &arg1)
{
    ui->lineEdit->setText(arg1);
}

void MainWindow::on_radioButton_clicked()
{
    QMessageBox::information(this, "Включено", ui->radioButton->text());
}

void MainWindow::on_radioButton_2_clicked()
{
    QMessageBox::information(this, "Включено", ui->radioButton_2->text());
}

void MainWindow::on_radioButton_3_clicked()
{
    QMessageBox::information(this, "Включено", ui->radioButton_3->text());
}

void MainWindow::on_pushButton_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,"Open Image File",QDir::currentPath(),tr("JPEG (*.jpg *.jpeg);;PNG (*.png)"));

       if(!fileName.isEmpty())
       {
           QImage image(fileName);

           if(image.isNull())
           {
               QMessageBox::information(this,"Image Viewer","Error Displaying image");
               return;
           }

           QGraphicsScene* scene = new QGraphicsScene(this);
           ui->graphicsView->setScene(scene);
           scene->addPixmap(QPixmap::fromImage(image));
           ui->graphicsView->fitInView(scene->sceneRect(),Qt::KeepAspectRatio);
        }
}

void MainWindow::on_pushButton_2_clicked()
{
    if(ui->lineEdit_2->text().isEmpty()){
        ui->label->setText("А там ничего не написано");
    } else {
        ui->label->setText(ui->lineEdit_2->text());
    }
}

void MainWindow::on_pushButton_3_clicked()
{
    ui->label->setText("Установить то, что написано выше вместо этого текста");
}

void MainWindow::on_pushButton_5_clicked()
{
    ui->textEdit->setText("");
}

void MainWindow::on_pushButton_4_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this,"Open Text File", QDir::currentPath(), "Text files (*.txt);;All (*)");
    if(!fileName.isEmpty()){
        QFile file(fileName);
        if(QFileInfo(fileName).suffix() == "txt" & file.open(QFile::ReadOnly | QFile::Text)){
            QTextStream  stream(&file);
            while (!stream.atEnd()){
                ui->textEdit->setText(stream.readAll());
            }
        } else {
            QMessageBox::critical(this, "Ошибка открытия файла", "Выбран не текстовый файл.");
        }
    }
}
